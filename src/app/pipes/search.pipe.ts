import { Pipe, PipeTransform } from '@angular/core';
import { Post } from '../models/post';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(posts: Post[], keyword: string) {
    if (keyword === '') {
      return posts;
    }
    return posts.filter(x => x.title.includes(keyword));
  }

}
