import { Directive } from '@angular/core';
import { ValidatorFn, AbstractControl } from '@angular/forms';

export function nameValidator(nameRe: RegExp): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} => {
    const test = nameRe.test(control.value);
    console.log(nameRe);
    console.log(control);
    return test ? { 'nameValidator': {value: control.value} } : null;
  };
}
@Directive({
  selector: '[appNameValidator]'
})
export class NameValidatorDirective {

  constructor() { }

}
