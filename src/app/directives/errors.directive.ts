import { Directive, ElementRef, Renderer } from '@angular/core';
@Directive({
  selector: '[appErrors]'
})
export class ErrorsDirective {

  constructor(el: ElementRef, renderer: Renderer) {
    renderer.setElementStyle(el.nativeElement, 'color', 'red');
   }

}
