import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { ListPostsComponent } from './components/list-posts/list-posts.component';
import { AppRoutingModule } from './app-routing';
import { PostsService } from './services/posts.service';
import { HttpClientModule } from '@angular/common/http';
import { PostDetailsComponent } from './components/post-details/post-details.component';
import { CreatePostComponent } from './components/create-post/create-post.component';
import { SearchPipe } from './pipes/search.pipe';
import { ErrorsDirective } from './directives/errors.directive';
import { NameValidatorDirective } from './directives/name-validator.directive';

@NgModule({
  declarations: [
    AppComponent,
    ListPostsComponent,
    PostDetailsComponent,
    CreatePostComponent,
    SearchPipe,
    ErrorsDirective,
    NameValidatorDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [PostsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
