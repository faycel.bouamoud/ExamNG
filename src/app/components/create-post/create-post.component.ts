import { Component, OnInit } from '@angular/core';
import { Post } from '../../models/post';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { nameValidator } from '../../directives/name-validator.directive';
@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.css']
})
export class CreatePostComponent implements OnInit {
  post: Post;
  postForm: FormGroup;
  constructor(private fb: FormBuilder) {
    this.post = new Post();
    this.create();
   }

  ngOnInit() {
  }

  create() {
    this.postForm = this.fb.group({
      title: ['', [Validators.required, nameValidator(/faycel/i)]],
      body: ['', Validators.maxLength(30)]
    });
  }

}
