import { Component, OnInit } from '@angular/core';
import { PostsService } from '../../services/posts.service';
import { Post } from '../../models/post';
import { Router } from '@angular/router';;

@Component({
  selector: 'app-list-posts',
  templateUrl: './list-posts.component.html',
  styleUrls: ['./list-posts.component.css'],
  providers: [PostsService]
})
export class ListPostsComponent implements OnInit {
  posts: Post[];
  keyword: string;
  isLoading = true;
  constructor(private postsService: PostsService, private router: Router) { }

  ngOnInit() {
    this.postsService.getPosts().subscribe(res => {
      this.posts = res;
      this.isLoading = false;
    });
  }

  showPost(id: number) {
    this.router.navigate(['posts', id]);
  }
}
