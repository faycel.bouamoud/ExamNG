import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Post } from '../../models/post';
import { ActivatedRoute } from '@angular/router';
import { PostsService } from '../../services/posts.service';

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.css']
})
export class PostDetailsComponent implements OnInit {
  post: Post;
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private postService: PostsService
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(res => {
      this.postService.getOnePost(res['id']).subscribe(ress => {
        this.post = ress;
      });
    });
  }

}
