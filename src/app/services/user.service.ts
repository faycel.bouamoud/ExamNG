import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class UserService {

  constructor(private http: HttpClient) { }
    public getUsers(): Observable<User[]> {
      return this.http.get<User[]>('http://localhost:3000/users');
    }
    public getOnePost(id: number): Observable<User> {
      return this.http.get<User>('http://localhost:3000/users/' + id);
    }
}
