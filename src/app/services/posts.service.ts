import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { Post } from '../models/post';

@Injectable()
export class PostsService {

  constructor(private http: HttpClient) { }

  public getPosts(): Observable<Post[]> {
    return this.http.get<Post[]>('https://jsonplaceholder.typicode.com/posts');
  }

  public getOnePost(id: number): Observable<Post> {
    return this.http.get<Post>('https://jsonplaceholder.typicode.com/posts/' + id);
  }

  public deletePost(id: number) {
    return this.http.delete('https://jsonplaceholder.typicode.com/posts/' + id);
  }

  public create(post: Post) {
    return this.http.post('https://jsonplaceholder.typicode.com/posts', post);
  }
}
